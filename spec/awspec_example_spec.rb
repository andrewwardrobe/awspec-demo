require 'spec_helper'

describe vpc('main') do
  it { should exist }
  it { should be_available }
  its(:vpc_id) { should eq 'vpc-077ad269337021cb5' }
  its(:cidr_block) { should eq '10.0.0.0/24' }
  it { should have_route_table('Private Route Table') }
  it { should have_route_table('Default Route Table') }
  it { should have_route_table('Public Route Table') }
  it { should have_network_acl('Public NACL') }
  it { should have_network_acl('Default NACL') }
  it { should have_network_acl('Private NACL') }
end


describe eip(ENV['EIP_VPN']) do
  it { should exist }
  it { should be_associated_to('i-0bfe4fc3b485bbc67') }
  it { should belong_to_domain('vpc') }
end

describe eip(ENV['EIP_NAT']) do
  it { should exist }
  it { should be_associated_to('') }
  it { should belong_to_domain('vpc') }
end

describe nat_gateway('nat-09f097b21274eb9c3') do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  it { should have_eip(ENV['EIP_NAT']) }
end

describe ec2('openvpn') do
  it { should exist }
  it { should be_running }
  its(:instance_id) { should eq 'i-0bfe4fc3b485bbc67' }
  its(:image_id) { should eq 'ami-6ec5d30a' }
  its(:private_dns_name) { should eq 'ip-10-0-0-42.eu-west-2.compute.internal' }
  its(:public_dns_name) { should eq '' }
  its(:instance_type) { should eq 't2.small' }
  its(:private_ip_address) { should eq '10.0.0.42' }
  its(:public_ip_address) { should eq ENV['EIP_VPN'] }
  it { should have_security_group('terraform-20180425080553844300000001') }
  it { should belong_to_vpc('main') }
  it { should belong_to_subnet('Public Subnet') }
  it { should have_eip(ENV['EIP_VPN']) }
  it { should have_ebs('vol-05c4573915681f6a6') }
  it { should have_network_interface('eni-08a07967aaa62df4f') }
end

describe internet_gateway('Internet Gateway') do
  it { should exist }
  it { should be_attached_to('main') }
end

describe network_acl('Public NACL') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_subnet('Public Subnet') }
  its(:outbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:outbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:inbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound_entries_count) { should eq 2 }
  its(:outbound_entries_count) { should eq 2 }
end

describe network_acl('Default NACL') do
  it { should exist }
  it { should belong_to_vpc('main') }
  its(:outbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound_entries_count) { should eq 1 }
  its(:outbound_entries_count) { should eq 1 }
end

describe network_acl('Private NACL') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_subnet('Private Subnet') }
  its(:outbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:outbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound) { should be_allowed.protocol('ALL').source('0.0.0.0/0').rule_number(100) }
  its(:inbound) { should be_denied.protocol('ALL').source('0.0.0.0/0').rule_number('*') }
  its(:inbound_entries_count) { should eq 2 }
  its(:outbound_entries_count) { should eq 2 }
end

describe subnet('Private Subnet') do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  its(:cidr_block) { should eq '10.0.0.128/25' }
end

describe subnet('Public Subnet') do
  it { should exist }
  it { should be_available }
  it { should belong_to_vpc('main') }
  its(:cidr_block) { should eq '10.0.0.0/25' }
end

describe security_group('Private Security Group') do
  it { should exist }
  its(:group_id) { should eq 'sg-0066514dfa65b917d' }
  its(:group_name) { should eq 'terraform-20180425080553858700000002' }
  its(:inbound) { should be_opened(22).protocol('tcp').for('0.0.0.0/0') }
  its(:inbound_rule_count) { should eq 1 }
  its(:outbound_rule_count) { should eq 0 }
  its(:inbound_permissions_count) { should eq 1 }
  its(:outbound_permissions_count) { should eq 0 }
  it { should belong_to_vpc('main') }
end

describe security_group('Public Security Group') do
  it { should exist }
  its(:group_id) { should eq 'sg-0965144cc5352365a' }
  its(:group_name) { should eq 'terraform-20180425080553844300000001' }
  its(:inbound) { should be_opened(1194).protocol('udp').for('0.0.0.0/0') }
  its(:inbound) { should be_opened(22).protocol('tcp').for('0.0.0.0/0') }
  its(:inbound) { should be_opened(443).protocol('tcp').for('0.0.0.0/0') }
  its(:inbound_rule_count) { should eq 3 }
  its(:outbound_rule_count) { should eq 0 }
  its(:inbound_permissions_count) { should eq 3 }
  its(:outbound_permissions_count) { should eq 0 }
  it { should belong_to_vpc('main') }
end

describe route_table('Private Route Table') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_route('10.0.0.0/24').target(gateway: 'local') }
  it { should have_route('0.0.0.0/0').target(nat: 'nat-09f097b21274eb9c3') }
  it { should have_subnet('Private Subnet') }
end

describe route_table('Default Route Table') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_route('10.0.0.0/24').target(gateway: 'local') }
end

describe route_table('Public Route Table') do
  it { should exist }
  it { should belong_to_vpc('main') }
  it { should have_route('10.0.0.0/24').target(gateway: 'local') }
  it { should have_route('0.0.0.0/0').target(gateway: 'igw-0db5356cfeb6aecfc') }
  it { should have_subnet('Public Subnet') }
end
