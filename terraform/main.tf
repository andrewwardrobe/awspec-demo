terraform {
  backend "s3" {
  }
}

data "terraform_remote_state" "state" {
  backend = "s3"
  config {
    bucket     = "${var.state_bucket}"
    region     = "${var.region}"
    key        = "${var.state_file}"
  }
}

provider "aws" {
  region = "${var.region}"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/24"
  tags {
    Name = "main"
  }
}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = "${aws_vpc.main.default_network_acl_id}"
  tags {
    Name = "Default NACL"
  }
}

resource "aws_default_route_table" "default" {
  default_route_table_id = "${aws_vpc.main.default_route_table_id}"
  tags {
    Name = "Default Route Table"
  }
}

resource "aws_subnet" "public_subnet" {
  cidr_block = "${cidrsubnet(aws_vpc.main.cidr_block ,1 ,0 )}"
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "Public Subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  cidr_block = "${cidrsubnet(aws_vpc.main.cidr_block ,1 ,1 )}"
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "Private Subnet"
  }
}


resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_eip" "openVPN" {
  vpc = true
  instance = "${aws_instance.openVPN.id}"
}


# Internet access
resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Network = "Public"
    Name = "Internet Gateway"
  }
}

resource "aws_nat_gateway" "main" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id = "${aws_subnet.private_subnet.id}"
  tags {
    Name = "NAT-Gateway"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "Public Route Table"
  }
}


resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.main.id}"
  tags {
    Name = "Private Route Table"
  }
}

resource "aws_route" "public" {
  route_table_id = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.main.id}"
}

resource "aws_route_table_association" "public" {
  route_table_id = "${aws_route_table.public.id}"
  subnet_id = "${aws_subnet.public_subnet.id}"
}



resource "aws_route" "private" {
  route_table_id = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = "${aws_nat_gateway.main.id}"

}

resource "aws_route_table_association" "private" {
  route_table_id = "${aws_route_table.private.id}"
  subnet_id = "${aws_subnet.private_subnet.id}"
}

resource "aws_network_acl" "public" {
  vpc_id = "${aws_vpc.main.id}"
  subnet_ids = ["${aws_subnet.public_subnet.id}"]
  egress {
    rule_no = "100"
    from_port = "0"
    to_port = "0"
    action = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol = "-1"
  }
  ingress {
    rule_no = "100"
    from_port = "0"
    to_port = "0"
    action = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol = "-1"
  }
  tags {
    Name = "Public NACL"
  }
}

resource "aws_network_acl" "private" {
  vpc_id = "${aws_vpc.main.id}"
  subnet_ids = ["${aws_subnet.private_subnet.id}"]
  egress {
    rule_no = "100"
    from_port = "0"
    to_port = "0"
    action = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol = "-1"
  }
  ingress {
    rule_no = "100"
    from_port = "0"
    to_port = "0"
    action = "ALLOW"
    cidr_block = "0.0.0.0/0"
    protocol = "-1"
  }
  tags {
    Name = "Private NACL"
  }
}

resource "aws_security_group" "public" {
 vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = "443"
    to_port = "443"
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "tcp"
  }
  ingress {
    from_port = "22"
    to_port = "22"
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "tcp"
  }
  ingress {
    from_port = "1194"
    to_port = "1194"
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "udp"
  }
  tags {
    Name = "Public Security Group"
  }
}

resource "aws_security_group" "private" {
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = "22"
    to_port = "22"
    cidr_blocks = ["0.0.0.0/0"]
    protocol = "tcp"
  }
  tags {
    Name = "Private Security Group"
  }
}

resource "aws_instance" "openVPN" {
  ami = "ami-6ec5d30a"
  instance_type = "t2.small"
  user_data = ""
  subnet_id = "${aws_subnet.public_subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.public.id}"]
  tags {
    Name = "openvpn"
  }
}

